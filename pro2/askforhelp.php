
<?php
	session_start();
	require 'db.php';
	$logged_in = False;
	if (isset($_SESSION['logged_userID'])) {
		$logged_in = True;
		$id = $_SESSION['logged_userID'];
		$query = "SELECT name,mobile_number,work,email FROM users WHERE id ='$id' ";
		$result = $con->query($query)->fetch_assoc();
		$user_name = $result['name'];
		$mobile_number = $result['mobile_number'];
		$work = $result['work'];
		$email = $result['email'];
	}
?>

<!--get_header('Page Name','Title')-->
<!doctype html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>Help4COVID</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CRoboto+Slab:400,700" rel="stylesheet">

		<link rel="icon" type="image/png" href="favicon.ico.html">
		<!-- Place favicon.ico in the root directory -->
		<link rel="apple-touch-icon" href="apple-touch-icon.png.html">

		<link rel="stylesheet" href="assets/css/font-awesome.min.css">

		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/css/xsIcon.css">
		<link rel="stylesheet" href="assets/css/isotope.css">
		<link rel="stylesheet" href="assets/css/magnific-popup.css">
		<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
		<link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
		<link rel="stylesheet" href="assets/css/animate.css">
		

		<!--For Plugins external css-->
		<link rel="stylesheet" href="assets/css/plugins.css" />

		<!--Theme custom css -->
		<link rel="stylesheet" href="assets/css/style.css">

		<!--Theme Responsive css-->
		<link rel="stylesheet" href="assets/css/responsive.css" />
		
        <link rel="stylesheet" href="cards/style.css">
	</head>
	<body>
	<!--[if lt IE 10]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->

	<div id="preloader">
		<div class="spinner">
			<div class="double-bounce1"></div>
			<div class="double-bounce2"></div>
		</div>
	</div><!-- #preloader -->

<!-- header section -->
<header class="xs-header header-transparent">
	<div class="container">
		<nav class="xs-menus">
			<div class="nav-header">
				<div class="nav-toggle"></div>
				<a href="index.php" class="nav-logo">
					<img src="assets/images/logo.png" alt="">
				</a>
			</div><!-- .nav-header END -->
			<div class="nav-menus-wrapper row">
				<div class="xs-logo-wraper col-lg-2 xs-padding-0">
					<a class="nav-brand" href="index.php">
						<img src="assets/images/logo-1.png" alt="">
					</a>
				</div><!-- .xs-logo-wraper END -->
				<div class="col-lg-7">
					<ul class="nav-menu">
						<li><a href="index.php#">Home</a>
							
						</li>
						<li><a href="askforhelp.php">Ask help</a></li>
						<li><a href="selection.php">Help now!</a></li>
						
						<li><a href="volunteer_register.php">become volunteer</a></li>
						<li><a href="idea_selection.php">Ideas</a></li>
						<li><a href="art_selection.php">Creations</a></li>
						
						
					</ul><!-- .nav-menu END -->
				</div>
				<div class="xs-navs-button d-flex-center-end col-lg-3">
				<?php
					if($logged_in){
				?>
					<a href="profile.php" class="btn btn-primary">
						<span class="badge"><i class="fa fa-user"></i></span><?php echo $user_name; ?>
					</a>
				<?php
					}
					else{
				?>
					<a href="login.php" class="btn btn-primary">
						<span class="badge"><i class="fa fa-user"></i></span> login
					</a>				
				<?php
					}
				?>
				</div><!-- .xs-navs-button END -->
			</div><!-- .nav-menus-wrapper .row END -->
		</nav><!-- .xs-menus .fundpress-menu END -->
	</div><!-- .container end -->
</header><!-- End header section -->

<!-- welcome section -->
<!--breadcumb start here-->
<section class="xs-banner-inner-section parallax-window" style="background-image:url('assets/images/backgrounds/help.jpg')">
	<div class="xs-black-overlay"></div>
	<div class="container">
		<div class="color-white xs-inner-banner-content">
			<h2>Ask Help</h2>
			<p>Please be safe and ask for help from others. </p>
			<ul class="xs-breadcumb">
				<li class="badge badge-pill badge-primary"><a href="index.php" class="color-white"> Home /</a>Ask Help</li>
			</ul>
		</div>
	</div>
</section>
<!--breadcumb end here--><!-- End welcome section -->


<main class="xs-main">
	<!-- contact section -->
	<section class="xs-contact-section-v2">
	<div class="container">
	<div class="xs-contact-container">
		<div class="row">
			<div class="col-lg-6">
				<div class="xs-volunteer-form-wraper bg-aqua">
					<h2>What do you need ?</h2>
					<!-- <p>It only takes a minute to set up a campaign. Decide what to do. Pick a name. Pick a photo. And just like that, you’ll be ready to start raising money.</p>
					 -->
						<?php
							if($logged_in){
						?>
							<form action="insert_askhelp.php" method="POST" enctype="multipart/form-data" id="volunteer-form" class="xs-volunteer-form">
								<div class="row">
									<div class="col-lg-6">
										<select name="help" required class="form-control" id="volunteer_brach">
											<option value="">Type of help</option>	
											<option value="">Food</option>
											<option value="">Medicine</option>
											<option value="">donation</option>
											<option value="">Water</option>
											<option value="">Cloths</option>
											<option value="">Hospital equipment</option>
											<option value="">Others</option>
										</select>
										
									</div>
									<div class="col-lg-6">
										<input type="text" required name="need" id="volunteer_name" class="form-control" placeholder="What is your need">
									</div>
									
									<div class="col-lg-6 xs-mb-20">
										<div class="xs-fileContainer">
											<input type="file" id="volunteer_cv" class="form-control" name="file">
											<label for="volunteer_cv">Upload pic</label>
										</div>
									</div>
									<div class="col-lg-6">
										<select name="country" required class="countries order-alpha" id="countryId">
		    						<option value="">Select Country</option>
									</select>

									</div>

									<div class="col-lg-6">
										<select name="state" required class="states order-alpha" id="stateId">
										    <option value="">Select State</option>
										</select>
									</div>
									<br><br><br>
									<div class="col-lg-6">
										<select name="city" required class="cities order-alpha" id="cityId">
									    	<option value="">Select City</option>
										</select>
									</div>

									<div class="col-lg-6">
										<textarea name="address" required id="massage" placeholder="Your Address" cols="30" class="form-control" rows="10"></textarea>
									</div>

									<div class="col-lg-6">
										<textarea name="about_need" required id="massage" placeholder="Write about your need(upto 40 words)" cols="30" class="form-control" rows="10"></textarea>
									</div>
								</div>
								<button type="submit" class="btn btn-primary">Submit</button>
							</form>
						<?php
							}
							else{
						?>
							<form action="insert_askhelp.php" method="POST" enctype="multipart/form-data" id="volunteer-form" class="xs-volunteer-form">
								<div class="row">
									<div class="col-lg-6">
											<input type="text" name="name" required id="volunteer_name" class="form-control" placeholder="Your Name">
									</div>
									<div class="col-lg-6">
										<input type="text" name="mobile_number" required id="volunteer_email" class="form-control" placeholder="Your Contact number">
									</div>
									<div class="col-lg-6">
										<select name="work" required class="form-control" id="volunteer_brach">
											<option value="">Who are you</option>	
											<option value="">Doctor/Nurse/Paramedic</option>
											<option value="">Police/Officers</option>
											<option value="">Student</option>
											<option value="">Retailer</option>
											<option value="">Delivery Agent</option>
											<option value="">Wholesaler</option>
											<option value="">Others</option>
										</select>
									</div>
									<div class="col-lg-6">
										<select name="help" required class="form-control" id="volunteer_brach">
											<option value="">Type of help</option>	
											<option value="">Food</option>
											<option value="">Medicine</option>
											<option value="">donation</option>
											<option value="">Water</option>
											<option value="">Cloths</option>
											<option value="">Hospital equipment</option>
											<option value="">Others</option>
										</select>
										
									</div>
									<div class="col-lg-6">
										<input type="text" name="need" required id="volunteer_name" class="form-control" placeholder="What is your need">
									</div>
									<div class="col-lg-6">
										<input type="email" name="email" required id="volunteer_email" class="form-control" placeholder="Your Email">
									</div>
									
									<div class="col-lg-6 xs-mb-20">
										<div class="xs-fileContainer">
											<input required type="file" id="volunteer_cv" class="form-control" name="file">
											<label for="volunteer_cv">Upload pic</label>
										</div>
									</div>
									<div class="col-lg-6">
										<select required name="country" class="countries order-alpha" id="countryId">
		    						<option value="">Select Country</option>
									</select>

									</div>

									<div class="col-lg-6">
										<select required name="state" class="states order-alpha" id="stateId">
										    <option value="">Select State</option>
										</select>
									</div>
									<br><br><br>
									<div class="col-lg-6">
										<select required name="city" class="cities order-alpha" id="cityId">
									    	<option value="">Select City</option>
										</select>
									</div>

									<div class="col-lg-6">
										<textarea required name="address" id="massage" placeholder="Your Address" cols="30" class="form-control" rows="10"></textarea>
									</div>

									<div class="col-lg-6">
										<textarea name="about_need" id="massage" placeholder="Write about your need(upto 40 words)" cols="30" class="form-control" rows="10"></textarea>
									</div>
								</div>
								<button type="submit" class="btn btn-primary">Submit</button>
							</form>
						<?php
							}
						?>
				</div>
			</div>
					<!-- #volunteer-form .xs-volunteer-form END -->
									<div class="col-lg-6">
					<div class="xs-maps-wraper map-wraper-v2">
						<br>
					<img src="assets/images/ask.png">
					</div>
			</div>
			</div>
				</div>
				</div>
			</div>
		</div><!-- .row end -->
	</div><!-- .container end -->
<!-- contact details section -->
</section>
		</main>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
<script src="//geodata.solutions/includes/countrystatecity.js"></script>

		<script src="assets/js/jquery-3.2.1.min.js"></script>
		<script src="assets/js/plugins.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/isotope.pkgd.min.js"></script>
		<script src="assets/js/jquery.magnific-popup.min.js"></script>
		<script src="assets/js/owl.carousel.min.js"></script>
		<script src="assets/js/jquery.waypoints.min.js"></script>
		<script src="assets/js/jquery.countdown.min.js"></script>
		<script src="assets/js/spectragram.min.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?v=3&amp;key=AIzaSyCy7becgYuLwns3uumNm6WdBYkBpLfy44k"></script>

		<script src="assets/js/main.js"></script>
	</body>
</html>
<!-- footer section end